const colorList = ["red", "yellow", "blue", "white", "green", "orange", "pink", "grey", "purple"];

const renderButton = () => {
    let contentHTML = '';
    for (let i = 0; i < colorList.length; i++) {
        const color = colorList[i];
        contentHTML += `<button 
        class="btn btn-primary" 
        style="background-color: ${color}"
        onclick="changeColor('${color}')">
        ${color}
        </button>`
    }
    document.getElementById('colorContainer').innerHTML = contentHTML;
}
renderButton();

const changeColor = (color) => {
    document.getElementById('house').style.color = color;
    document.getElementById('house1').style.backgroundColor = color;
    document.getElementById('house2').style.backgroundColor = color;
    document.getElementById('house3').style.backgroundColor = color;
    document.getElementById('house4').style.backgroundColor = color;


}